﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KavosAparatas1
{
    public partial class KavosAparatas : Form
    {
        public KavosAparatas()
        {
            InitializeComponent();
        }

        private void KavosAparatas_Load(object sender, EventArgs e)
        {

        }

        private void Juoda_Click(object sender, EventArgs e)
        {
            string kava = "Juoda";
            kokiaKava(kava);
        }

        private void Balta_Click(object sender, EventArgs e)
        {
            string kava = "Balta";
            kokiaKava(kava);
        }

        private void Late_Click(object sender, EventArgs e)
        {
            string kava = "Late";
            kokiaKava(kava);
        }

        private void Mocha_Click(object sender, EventArgs e)
        {
            string kava = "Mocha";
            kokiaKava(kava);
        }

        private void Capuchino_Click(object sender, EventArgs e)
        {
            string kava = "Capuchino";
            kokiaKava(kava);
        }
        private void kokiaKava(string kava)
        {
            Ekranas.Text = kava + " kava pagaminta.";
        }

        private void Ekranas_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
