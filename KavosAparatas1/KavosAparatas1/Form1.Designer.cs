﻿namespace KavosAparatas1
{
    partial class KavosAparatas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Juoda = new System.Windows.Forms.Button();
            this.Balta = new System.Windows.Forms.Button();
            this.Late = new System.Windows.Forms.Button();
            this.Mocha = new System.Windows.Forms.Button();
            this.Capuchino = new System.Windows.Forms.Button();
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Juoda
            // 
            this.Juoda.Location = new System.Drawing.Point(21, 75);
            this.Juoda.Name = "Juoda";
            this.Juoda.Size = new System.Drawing.Size(75, 23);
            this.Juoda.TabIndex = 0;
            this.Juoda.Text = "Juoda";
            this.Juoda.UseVisualStyleBackColor = true;
            this.Juoda.Click += new System.EventHandler(this.Juoda_Click);
            // 
            // Balta
            // 
            this.Balta.Location = new System.Drawing.Point(102, 75);
            this.Balta.Name = "Balta";
            this.Balta.Size = new System.Drawing.Size(75, 23);
            this.Balta.TabIndex = 1;
            this.Balta.Text = "Balta";
            this.Balta.UseVisualStyleBackColor = true;
            this.Balta.Click += new System.EventHandler(this.Balta_Click);
            // 
            // Late
            // 
            this.Late.Location = new System.Drawing.Point(20, 104);
            this.Late.Name = "Late";
            this.Late.Size = new System.Drawing.Size(75, 23);
            this.Late.TabIndex = 2;
            this.Late.Text = "Late";
            this.Late.UseVisualStyleBackColor = true;
            this.Late.Click += new System.EventHandler(this.Late_Click);
            // 
            // Mocha
            // 
            this.Mocha.Location = new System.Drawing.Point(101, 104);
            this.Mocha.Name = "Mocha";
            this.Mocha.Size = new System.Drawing.Size(75, 23);
            this.Mocha.TabIndex = 3;
            this.Mocha.Text = "Mocha";
            this.Mocha.UseVisualStyleBackColor = true;
            this.Mocha.Click += new System.EventHandler(this.Mocha_Click);
            // 
            // Capuchino
            // 
            this.Capuchino.Location = new System.Drawing.Point(21, 133);
            this.Capuchino.Name = "Capuchino";
            this.Capuchino.Size = new System.Drawing.Size(75, 23);
            this.Capuchino.TabIndex = 4;
            this.Capuchino.Text = "Capuchino";
            this.Capuchino.UseVisualStyleBackColor = true;
            this.Capuchino.Click += new System.EventHandler(this.Capuchino_Click);
            // 
            // Ekranas
            // 
            this.Ekranas.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Ekranas.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Ekranas.Location = new System.Drawing.Point(21, 29);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(200, 20);
            this.Ekranas.TabIndex = 5;
            this.Ekranas.TextChanged += new System.EventHandler(this.Ekranas_TextChanged);
            // 
            // KavosAparatas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Ekranas);
            this.Controls.Add(this.Capuchino);
            this.Controls.Add(this.Mocha);
            this.Controls.Add(this.Late);
            this.Controls.Add(this.Balta);
            this.Controls.Add(this.Juoda);
            this.Name = "KavosAparatas";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.KavosAparatas_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Juoda;
        private System.Windows.Forms.Button Balta;
        private System.Windows.Forms.Button Late;
        private System.Windows.Forms.Button Mocha;
        private System.Windows.Forms.Button Capuchino;
        private System.Windows.Forms.TextBox Ekranas;
    }
}

